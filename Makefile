##
# VCV Rack fedora package
#

vcv-rack.tar.gz:
	git submodule update --init --recursive
	cd vcv-rack; tar czf ../vcv-rack.tar.gz ./*

# end
