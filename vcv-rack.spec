Name:           vcv-rack
Version:        1.1.6
Release:        1%{?dist}
Summary:        Virtual Eurorack DAW

License:        GPLv3
URL:            https://vcvrack.com/
Source0:        vcv-rack.tar.gz
Source1:        VCV-Rack.desktop
Source2:        vcv-rack-exe
Patch0:         10-link-to-syslibs.patch

BuildRequires:  make
BuildRequires:  gcc-c++
BuildRequires:  wget
BuildRequires:  glew-devel
BuildRequires:  glfw-devel
BuildRequires:  jansson-devel
BuildRequires:  libcurl-devel
BuildRequires:  openssl-libs
BuildRequires:  openssl-devel
BuildRequires:  libzip-devel
BuildRequires:  zlib-devel
BuildRequires:  speexdsp-devel
BuildRequires:  libsamplerate-devel
BuildRequires:  rtaudio-devel
BuildRequires:  rtmidi-devel
BuildRequires:  git
BuildRequires:  gdb
BuildRequires:  curl
BuildRequires:  cmake
BuildRequires:  libX11-devel
BuildRequires:  mesa-libGLU-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXi-devel
BuildRequires:  alsa-lib-devel
BuildRequires:  gtk2-devel
BuildRequires:  jack-audio-connection-kit-devel
BuildRequires:  jq

%description


%prep
%setup -c vcv-rack-${version}
%patch0 -p0


%build
pushd dep
mkdir include
make include/nanovg.h
make include/nanosvg.h
make include/blendish.h
make include/osdialog.h
make include/pffft.h
popd
%make_build


%install
mkdir -p %{buildroot}/opt/vcv-rack
cp -r Rack Core.json cacert.pem res %{buildroot}/opt/vcv-rack
chmod +x %{buildroot}/opt/vcv-rack/Rack

mkdir -p %{buildroot}%{_bindir}
cp %{SOURCE2} %{buildroot}%{_bindir}/vcv-rack
chmod +x %{buildroot}%{_bindir}

desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%files
/opt/vcv-rack
%{_bindir}
%{_datadir}
%license LICENSE.md LICENSE-dist.txt LICENSE-GPLv3.txt
%doc CHANGELOG.md



%changelog
* Mon Jun 15 2020 root
- 
